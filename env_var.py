import os

print "\n".join(["%s = %s" % (k, v) for k, v in os.environ.items()])